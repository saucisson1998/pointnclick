﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using Music;
using Player;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Dialogues
{
    public class DialogueManager : MonoBehaviour
    {
        public Text pnjName;
        public DialogueSO dialogue;
        public List<Button> buttons = new List<Button>(4);
        public AudioClip atoPnjAudio;
        public AudioClip pnjAudio;
        public TextMeshProUGUI pnjDial;
        public Inventory.Inventory inventory;
        public List<AudioClip> choicesAudio = new List<AudioClip>();
        public List<TextMeshProUGUI> ChoicesTexts;
        public AudioClip leaveAudio;
        public AudioManager _audioManager;
        public GameObject player;
        public int _currentCharacterIndex;
        public string _wholeDialog;
        public Image PNJImage;
        
        private float _timer=0;
        private float _timeLapse;
        // Start is called before the first frame update
        private void Awake()
        {
            _audioManager = GetComponent<AudioManager>();
        }

        private void OnDisable()
        {
            pnjDial.SetText("");
            HideButtons();
        }

        void OnEnable()
        {
            HideButtons();
            atoPnjAudio = dialogue.AToPNJAudio;
            pnjAudio = dialogue.PNJAudio;
            PNJImage.sprite = dialogue.PNJSprite;
            _audioManager.Add(atoPnjAudio);
            _audioManager.CallBack += InitializeDialogue;
        }

        void InitializeDialogue()
        {
            _audioManager.Add(pnjAudio);
            _currentCharacterIndex = 0;
            _wholeDialog = dialogue.PNJDial;
            SetTextNAudio();
        }
        // Update is called once per frame
        void Update()
        {
            SetTextNAudio();
            if (IsTyping()) TypeWritter();
            else if (_currentCharacterIndex == _wholeDialog.Length || _wholeDialog.Length !=0) ShowButtons();
        }

        void TypeWritter()
        {
            HideButtons();
            _timer += Time.deltaTime;
            if ( _timer >= _timeLapse ) {
                ++_currentCharacterIndex;
                pnjDial.text=_wholeDialog.Substring( 0, _currentCharacterIndex);
                _timer = 0f;
            }
           
        }
        
        private bool IsTyping()
        {
            return _currentCharacterIndex < _wholeDialog.Length;
        }

        /// <summary>
        /// Close DialogueCanvas
        /// </summary>
        private void CloseDialogue()
        {
            gameObject.SetActive(false);
            foreach (var button in buttons) button.interactable = true;
            HideButtons();
            player.GetComponent<InteractingComponent>().InUI = false;
        }

        void SelectDialogue(DialogueSO dial)
        {
            dialogue = dial;
        }

        void SetTextNAudio()
        {
            pnjName.text = dialogue.PNJName;
            pnjAudio = dialogue.PNJAudio;
            foreach (var choicesText in ChoicesTexts) 
                choicesText.SetText(dialogue.DialogueChoices[ChoicesTexts.IndexOf(choicesText)]);
            leaveAudio = dialogue.LeaveAudio;
        }

        public void PlayAudio(DialogueSO dial)
        {
            _audioManager.Stop();
            _audioManager.Add(dial.PNJAudio);
        }

        /// <summary>
        /// Play linked audio when your mouse is over a button
        /// </summary>
        /// <param name="overed">Button overed by mouse</param>
        public void ChoiceDial(Button overed)
        {
            _audioManager.ClearQueue();
            _audioManager.Add(choicesAudio[buttons.IndexOf(overed)]);
        }
        
        /// <summary>
        /// Choose the dialogue dependant of the clicked button
        /// </summary>
        /// <param name="clicked"> Button you clicked </param>
        public void ChoicePnjDial(Button clicked)
        {
            if (buttons.IndexOf(clicked) == 3)
            {
                UnactiveButtonsInteractions();
                _audioManager.ClearQueue();
                _audioManager.Add(leaveAudio);
                _audioManager.CallBack += CloseDialogue;
            }
            else
            {
                if (buttons.IndexOf(clicked )==2 && pnjName.text == "Don Pipistrello")
                {
                    SelectDialogue(dialogue.NextDialogue[buttons.IndexOf(clicked)]);
                    if (dialogue.NextDialogue[buttons.IndexOf(clicked)].ItemToGive != null) inventory.AddItem(dialogue.NextDialogue[buttons.IndexOf(clicked)].ItemToGive); 
                    _timeLapse = SetTimeLapse(dialogue.PNJDial, (dialogue.NextDialogue[buttons.IndexOf(clicked)].PNJAudio));
                    pnjDial.text = "";
                    PlayAudio(dialogue.NextDialogue[buttons.IndexOf(clicked)]);
                    _wholeDialog = dialogue.PNJDial;
                    _currentCharacterIndex = 0;
                    ActiveButtonsInteractions();
                    clicked.interactable = false;
                    _audioManager.CallBack += () => 
                    { 
                        GameObject.Find("DonPipistrello").SetActive(false); 
                        CloseDialogue();
                    };
                }
                SelectDialogue(dialogue.NextDialogue[buttons.IndexOf(clicked)]);
                if (dialogue.NextDialogue[buttons.IndexOf(clicked)].ItemToGive != null) inventory.AddItem(dialogue.NextDialogue[buttons.IndexOf(clicked)].ItemToGive); 
                _timeLapse = SetTimeLapse(dialogue.PNJDial, (dialogue.NextDialogue[buttons.IndexOf(clicked)].PNJAudio));
                pnjDial.text = "";
                PlayAudio(dialogue.NextDialogue[buttons.IndexOf(clicked)]);
                _wholeDialog = dialogue.PNJDial;
                _currentCharacterIndex = 0;
                ActiveButtonsInteractions();
                clicked.interactable = false;
            }
        }
        

        /// <summary>
        /// Set waiting time of Typewritter effect 
        /// </summary>
        /// <param name="textToWrite"> Text you want to write</param>
        /// <param name="linkedAudio"> Audio corresponding to the text</param>
        /// <returns></returns>
        private float SetTimeLapse(string textToWrite, AudioClip linkedAudio)
        {
            return (linkedAudio.length-1f) / textToWrite.Length;
        }

        /// <summary>
        /// Make all buttons invisible
        /// </summary>
        void HideButtons()
        {
            foreach (var button in buttons) button.gameObject.SetActive(false); 
        }

        /// <summary>
        /// Make all buttons visible
        /// </summary>
        void ShowButtons()
        {
            foreach (var button in buttons) button.gameObject.SetActive(true); 
        }

        /// <summary>
        /// Make all buttons interactable
        /// </summary>
        void ActiveButtonsInteractions()
        {
            foreach (var button in buttons)
            {
                button.interactable = true;
            }
        }
        void UnactiveButtonsInteractions()
        {
            foreach (var button in buttons)
            {
                button.interactable = false;
            }
        }
    }
}
