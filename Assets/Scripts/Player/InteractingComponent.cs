using Interaction;
using Inventory;
using UnityEngine;
using UnityEngine.AI;

namespace Player
{
    public class InteractingComponent : MonoBehaviour
    {

        private GameObject _lastSelectedGameObject;
        private NavMeshAgent _navMeshAgent;
        private bool _moveToInteractable = false;
        public Item SelectedItem;
        public Item Gun;
        public Inventory.Inventory Inventory;
        public bool InUI { get; set; }
        
        private void Start()
        {
            _navMeshAgent = GetComponent<NavMeshAgent>();
        }
        private void Update()
        {
            GetComponent<MovableComponent>().TargetPos = _navMeshAgent.destination;
            if (Input.GetMouseButtonDown(0)&& InUI == false)
            {
                RaycastHit hit;
                if (!Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, Mathf.Infinity)) return;
                if (hit.transform.GetComponent<Interactable>() != null)
                {
                    _moveToInteractable = true;
                    _lastSelectedGameObject = hit.transform.gameObject;
                    
                    _navMeshAgent.SetDestination(_lastSelectedGameObject.GetComponentInChildren<Interactable>().InteractionTarget.position);
                }
            }
            if (_moveToInteractable &&
                Vector3.Distance(transform.position, _lastSelectedGameObject.GetComponentInChildren<Interactable>().InteractionTarget.position)
                <= _navMeshAgent.stoppingDistance)
            {
                if (SelectedItem == _lastSelectedGameObject.GetComponentInChildren<Interactable>().CheckItem)
                {
                    _lastSelectedGameObject.GetComponentInChildren<Interactable>().Activated = true;
                    _lastSelectedGameObject.GetComponentInChildren<Interactable>().Use(gameObject); 
                    if(SelectedItem != Gun) Inventory.RemoveItem(SelectedItem);
                }
                if (SelectedItem != _lastSelectedGameObject.GetComponentInChildren<Interactable>().CheckItem && SelectedItem != null)
                {
                    if (_lastSelectedGameObject.GetComponentInChildren<PNJ>()) Inventory.ItemAudioManager.Add(SelectedItem.WrongPNJ);
                    else Inventory.ItemAudioManager.Add(SelectedItem.WrongDecor);
                    Inventory.UnselectItem();
                    Inventory.SwitchBool();
                }
                if (_lastSelectedGameObject.GetComponentInChildren<SaucissonPNJ>() != null)
                {
                    _lastSelectedGameObject.GetComponentInChildren<SaucissonPNJ>().PlayerItem = SelectedItem;
                    _lastSelectedGameObject.GetComponentInChildren<SaucissonPNJ>().Use(gameObject);
                    Inventory.RemoveItem(SelectedItem);
                    ClearSelectedItem();
                }

                if (_lastSelectedGameObject.GetComponentInChildren<EndGame>() != null)
                {
                    if (!GameObject.Find("DonPipistrello").activeInHierarchy) return;
                    _lastSelectedGameObject.GetComponentInChildren<SaucissonPNJ>().PlayerItem = SelectedItem;
                    _lastSelectedGameObject.GetComponentInChildren<SaucissonPNJ>().Use(gameObject);
                    Inventory.RemoveItem(SelectedItem);
                    ClearSelectedItem();
                }
                if (_lastSelectedGameObject.GetComponentInChildren<Interactable>().Activated)
                {
                    _lastSelectedGameObject.GetComponentInChildren<Interactable>().Use(gameObject);
                    ClearSelectedItem();
                } 
                
                else if (!_lastSelectedGameObject.GetComponentInChildren<Interactable>().Activated || SelectedItem !=null) Inventory.UnselectItem();
                ClearSelectedItem();
                _lastSelectedGameObject = null;
                _moveToInteractable = false;
            }
        }
        
        void ClearSelectedItem()
        {
            SelectedItem = null;
            Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
        }
    }
}