﻿using System.Collections;
using System.Collections.Generic;
using Dialogues;
using UnityEngine;
using UnityEngine.Video;

public class CreditManager : MonoBehaviour
{
    private AudioManager _audioManager;
    public VideoClip Credit;
    public AudioClip Music;
    public VideoPlayer VideoPlayer;
    
    
    private void Awake()
    {
        _audioManager = GetComponent<AudioManager>();
    }

    void Start()
    {
        _audioManager.Add(Music);
        VideoPlayer.clip = Credit;
        VideoPlayer.Play();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
