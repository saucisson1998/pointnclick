﻿using System.Linq;
using Dialogues;
using Inventory;
using Player;
using UnityEngine;

namespace Interaction
{
    public class Papistrello : PNJ
    {
        public Item KeyChecker;
        public override void Use(GameObject player)
        {
            base.Use(player);
            if (player.GetComponent<InteractingComponent>().Inventory.Items.Contains(KeyChecker))
            {
                DialogueCanvas.GetComponent<DialogueManager>().buttons[2].interactable = true;
            }
            else DialogueCanvas.GetComponent<DialogueManager>().buttons[2].interactable = false;
        }
    }
}
