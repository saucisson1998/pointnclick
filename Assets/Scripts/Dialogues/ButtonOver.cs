﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Dialogues
{
    public class ButtonOver : MonoBehaviour, IPointerClickHandler
    {
        public DialogueManager DialogueManager;
        
        public void OnPointerClick(PointerEventData eventData)
        {
            DialogueManager.ChoicePnjDial(GetComponent<Button>());
        }
    }
}
