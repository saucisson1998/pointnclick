﻿using System.Collections;
using Interaction;
using UnityEngine;
using UnityEngine.AI;

namespace Player
{
    public class MovableComponent : MonoBehaviour
    {
        private NavMeshAgent _navMeshAgent;
        public Camera MainCamera;
        private Vector3 _mousePos;
        private SpriteRenderer _sprite;
        private bool _flip;
        private InteractingComponent _interactingComponent;
        private Animator _animator;
        public Vector3 TargetPos;

        private float _speed;

        [Header("VALEURS DEPLACEMENTS")] [Range(0.1f, 5f)]
        public float MinScale;

        [Range(0.1f, 10f)] public float MinSpeed;
        [Range(1f, 10f)] public float MultiplierSpeed;
        [Range(0.01f, 0.5f)] public float MultiplierScale;

       
        void Start()
        {
            _navMeshAgent = GetComponentInChildren<NavMeshAgent>();
            _sprite = GetComponentInChildren<SpriteRenderer>();
            _interactingComponent = GetComponent<InteractingComponent>();
            _animator = GetComponentInChildren<Animator>();
            _navMeshAgent.speed = _speed;
            MainCamera = Camera.main;
        }

        // Update is called once per frame
        void Update()
        {
            _mousePos = Input.mousePosition;
            GetPositionInScene();
            ScalePlayer();
        }

        void GetPositionInScene()
        {
            if (_interactingComponent.InUI || Input.mousePosition.y < MainCamera.pixelHeight * 0.15) return;
            RaycastHit hit;
            if (Physics.Raycast(MainCamera.ScreenPointToRay(_mousePos), out hit, Mathf.Infinity))
            {
                Debug.DrawRay(hit.point, transform.TransformDirection(Vector3.up) * hit.distance, Color.red);
                Debug.Log("Did Hit");
            }

            if (Input.GetMouseButtonDown(0) && hit.collider.gameObject.GetComponentInChildren<Interactable>() == null)
            {
                TargetPos = hit.point;
                _navMeshAgent.SetDestination(TargetPos);
            }
            if (TargetPos.z < transform.position.z-5)
            {
                _animator.SetBool("Moving", true);
                _animator.SetBool("GoForward", false);
                _animator.SetBool("GoBackward", true);
            }
            if (Mathf.Abs(TargetPos.z - transform.position.z) <= 5)
            {
                _animator.SetBool("Moving", true);
                _animator.SetBool("GoForward", false);
                _animator.SetBool("GoBackward", false);
            }
            if (TargetPos.z > transform.position.z+5)
            {
                _animator.SetBool("Moving", true);
                _animator.SetBool("GoForward", true);
                _animator.SetBool("GoBackward", false);
            }
            if(Vector3.Distance(transform.position, TargetPos) <= _navMeshAgent.stoppingDistance+5 || TargetPos == Vector3.zero)
                StopAnim();
            if (Input.GetMouseButtonDown(0) && _flip == false && transform.localPosition.x < hit.point.x) Flip();
            if (Input.GetMouseButtonDown(0) && _flip && transform.localPosition.x > hit.point.x) Flip();
        }

        void ScalePlayer()
        {
            float zPosition = transform.position.z * -MultiplierScale;

            //Taille du sprite proportionnel à sa position dans le monde
            transform.localScale = new Vector3(zPosition + MinScale, zPosition + MinScale, zPosition + MinScale);

            //Vitesse du joueur proportionnel à sa taille avec un multiplicateur de vitesse pour changer la vitesse sans avoir a passer par l'IDE
            _navMeshAgent.speed = transform.localScale.y * MultiplierSpeed;

            if (transform.localScale.x < MinScale && transform.localScale.y < MinScale &&
                transform.localScale.z < MinScale)
                transform.localScale = new Vector3(MinScale, MinScale, MinScale);


            if (_navMeshAgent.speed < transform.position.z) _navMeshAgent.speed = MinSpeed;
        }

        void Flip()
        {
            _flip = !_flip;
            _sprite.flipX = !_sprite.flipX;
        }

        void StopAnim()
        {
            _animator.SetBool("Moving", false);
            _animator.SetBool("GoForward", false);
            _animator.SetBool("GoBackward", false);
        }
        
    }
}