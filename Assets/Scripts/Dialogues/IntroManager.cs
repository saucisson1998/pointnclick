﻿using Player;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Dialogues
{
    public class IntroManager : MonoBehaviour
    {
        public DialogueSO dialogue;
        public int _currentCharacterIndex;
        public string _wholeDialog;
        public AudioManager _audioManager;
        public TextMeshProUGUI pnjDial;
        public GameObject Player;
        
        private float _timer=0;
        private float _timeLapse;

        private void Awake()
        {
            _audioManager = GetComponent<AudioManager>();
        }

        private void OnEnable()
        {
            Player.GetComponent<InteractingComponent>().InUI = true;
            _timeLapse = SetTimeLapse(dialogue.PNJDial, dialogue.PNJAudio);
            _audioManager.Add(dialogue.PNJAudio);
            _wholeDialog = dialogue.PNJDial;
        }

        private void Update()
        {
            if (IsTyping()) TypeWritter();
        }

        private bool IsTyping()
        {
            return _currentCharacterIndex < _wholeDialog.Length;
        }

        void TypeWritter()
        {
            
            _timer += Time.deltaTime;
            if ( _timer >= _timeLapse ) {
                ++_currentCharacterIndex;
                pnjDial.text=_wholeDialog.Substring( 0, _currentCharacterIndex);
                _timer = 0f;
            }
           
        }
        
        private float SetTimeLapse(string textToWrite, AudioClip linkedAudio)
        {
            return (linkedAudio.length-1f) / textToWrite.Length;
        }
    }
}
