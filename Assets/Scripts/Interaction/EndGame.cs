﻿using System.Collections;
using System.Collections.Generic;
using Interaction;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndGame : Interactable
{
    public override void Use(GameObject player)
    {
        SceneManager.LoadSceneAsync(2);
    }
}
