﻿using System;
using System.Collections;
using System.Linq;
using Dialogues;
using Music;
using Player;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Inventory
{
    public class Inventory : MonoBehaviour
    {
        public Item DefaultItem;
        private const int SlotNumb = 5;
        public Image[] ItemImages = new Image[SlotNumb];
        public Item[] Items = new Item[SlotNumb];
        public Item CurrentlySelected;
        public Button[] SlotButtons = new Button[SlotNumb];
        public InteractingComponent InteractingComponent;
        public bool RemovableItem;
        public bool IsActive = false;
        public MusicManager MusicManager;
        public AudioManager ItemAudioManager;

        private void Awake()
        {
            ItemAudioManager = GetComponent<AudioManager>();
        }

        private void Start()
        {
            AddItem(DefaultItem);
        }
        
        private void Update()
        {
            foreach (var itemImage in ItemImages)
            {
                if (itemImage.sprite == null) itemImage.enabled = false;
            }

            for (int i = 0; i < SlotNumb; i++)
            {
                SlotButtons[i].GetComponent<ButtonClick>().CurrentItem = Items[i];
            }
        }

        public void SwitchBool()
        {
            IsActive = !IsActive;
        }

        public void AddItem(Item itemToAdd)
        {
            if (Items.Contains(itemToAdd)) return;
            for (int i = 0; i < SlotNumb; i++)
            {
                if (Items[i] == null)
                {
                    Items[i] = itemToAdd;
                    ItemImages[i].sprite = itemToAdd.IventoryItemImage;
                    ItemImages[i].enabled = true;
                    if (itemToAdd.MusicKey != "") MusicManager.SwitchMusic(itemToAdd.MusicKey);
                    if (itemToAdd.PickUp != null) ItemAudioManager.Add(itemToAdd.PickUp);
                    StartCoroutine(Blink(ItemImages[i]));
                    return;
                }
            }
        }

        public void RemoveItem(Item itemToRemove)
        {
            if (!RemovableItem)
            {
                for (int i = 0; i < SlotNumb; i++)
                {
                    if (Items[i] == itemToRemove)
                    {
                        Items[i] = null;
                        ItemImages[i].sprite = null;
                        ItemImages[i].enabled = false;
                        SwitchBool();
                        return;
                    }
                }
            }
        }
        
        public void SelectItem(Item item)
        {
            InteractingComponent.SelectedItem = item;
            CurrentlySelected = item;
            Debug.Log("Item : " + item.name + " Selectionné");
            Cursor.SetCursor(item.CursorTexture, Vector2.zero, CursorMode.Auto);
        }

        public void UnselectItem()
        {
            CurrentlySelected = null;
            InteractingComponent.SelectedItem = null;
            Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
        }
        
        IEnumerator Blink(Image sprite)
        {
            sprite.color = Color.red;
            yield return new WaitForSeconds(0.2f);
            sprite.color = Color.white;
            yield return new WaitForSeconds(0.2f);
            sprite.color = Color.red;
            yield return new WaitForSeconds(0.2f);
            sprite.color = Color.white;
        }
    }
}