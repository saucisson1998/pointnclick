﻿using System.Collections.Generic;
using Inventory;
using UnityEngine;
using UnityEngine.UI;

namespace Dialogues
{
    [CreateAssetMenu(fileName = "NewDialogue", menuName = "Interaction/NewDialogue")]
    public class DialogueSO : ScriptableObject
    {
        public string PNJName;
        public string PNJDial;
        public AudioClip AToPNJAudio;
        public AudioClip PNJAudio;
        public AudioClip LeaveAudio;
        public Item ItemToGive;
        public List<DialogueSO> NextDialogue = new List<DialogueSO>(3);
        public List<string> DialogueChoices = new List<string>();
        public Sprite PNJSprite;
    }
}
