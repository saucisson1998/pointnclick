﻿using Dialogues;
using Inventory;
using UnityEngine;

namespace Interaction
{
    public class SaucissonPNJ : PNJ
    {
        public Item SecondDialogueItem;
        public Item PlayerItem;
        public DialogueSO SecondDialogue;
        public override void Use(GameObject player)
        {
            if (PlayerItem == SecondDialogueItem)
            {
                LinkedDialogueSo = SecondDialogue;
                base.Use(player);
            }
            else base.Use(player);
        }
    }
}
