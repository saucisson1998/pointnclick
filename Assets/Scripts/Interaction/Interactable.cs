﻿using Inventory;
using Music;
using UnityEngine;

namespace Interaction
{
    public abstract class Interactable : MonoBehaviour
    {
        public Transform InteractionTarget;
        public Item CheckItem;
        public bool Activated = false;
        public virtual void Use(GameObject player)
        {
            
        }
    }
}
