﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

namespace Management
{
    public class NavigationManager : MonoBehaviour
    {
        public List<SpriteRenderer> ObstacleSpriteRenderers = new List<SpriteRenderer>();
        public GameObject Nav;
        public NavMeshSurface NavMeshSurface;
        // Start is called before the first frame update
        void Start()
        {
            BakeTerrain();
        }

        // Update is called once per frame
        void FixedUpdate()
        {
        
        }
        public void BakeTerrain()
        {
            foreach (SpriteRenderer ObstacleSpriteRenderer in ObstacleSpriteRenderers)
            {
                Nav = ObstacleSpriteRenderer.GetComponentInChildren<NavMeshModifier>().gameObject;
                if (Nav.GetComponent<MeshFilter>() != null)
                {
                    Destroy(Nav.GetComponent<MeshFilter>());
                }
            }
            foreach (SpriteRenderer ObstacleSpriteRenderer in ObstacleSpriteRenderers)
            {
                if(ObstacleSpriteRenderer.transform.parent.gameObject.activeSelf)
                {
                    Nav = ObstacleSpriteRenderer.GetComponentInChildren<NavMeshModifier>().gameObject;
                    Mesh mesh = new Mesh();
                    mesh.SetVertices(Array.ConvertAll(ObstacleSpriteRenderer.sprite.vertices, i => (Vector3)i).ToList());
                    mesh.SetUVs(0, ObstacleSpriteRenderer.sprite.uv.ToList());
                    mesh.SetTriangles(Array.ConvertAll(ObstacleSpriteRenderer.sprite.triangles, i => (int)i),0);
                    MeshFilter meshfilter = Nav.AddComponent<MeshFilter>();
                    meshfilter.mesh = mesh;
                }
            }
            NavMeshSurface.BuildNavMesh();
        }
    }
}
