﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Video;
using UnityEngine.UI;

namespace Menu
{
    public class MenuManager : MonoBehaviour
    {

        public VideoPlayer IntroMenuPlayer;
        public VideoClip IntroClip;
        public VideoPlayer MenuPlayer;
        public VideoClip LaunchClip;
        public GameObject ButtonsPanel;
        public List<Button> MenuButtons = new List<Button>();
        private float _buttonAlpha;
        private Color _imageColor;
        public AudioClip IntroMusic;
        public AudioClip MenuMusic;
        private AudioSource _musicPlayer;

        public GameObject Credits;
        public GameObject Menus;
        
        void Awake()
        {
            
            _musicPlayer = GetComponent<AudioSource>();
            _musicPlayer.clip = IntroMusic;
            _musicPlayer.Play();
            ButtonsPanel.SetActive(false);
            MenuPlayer.enabled = false;
            Cursor.visible = false;
        }

        // Update is called once per frame
        void Update()
        {
            if (IntroMenuPlayer.clip == IntroClip && (ulong) IntroMenuPlayer.frame == IntroMenuPlayer.frameCount - 10)
            {
                MenuPlayer.enabled = true;
                ButtonsPanel.SetActive(true);
                
            }
            
            if (IntroMenuPlayer.clip == IntroClip && (ulong) IntroMenuPlayer.frame == 102)
            {
                _musicPlayer.clip = MenuMusic;
                _musicPlayer.loop = true;
                _musicPlayer.Play();
            }
            

            if (IntroMenuPlayer.clip == IntroClip && (ulong) IntroMenuPlayer.frame == IntroMenuPlayer.frameCount)
            {
                IntroMenuPlayer.enabled = false;
                Cursor.visible = true;
            }
        }

        public void Play()
        {
            SceneManager.LoadSceneAsync(1);
        }

        public void Credit()
        {
            Credits.SetActive(true);
            Menus.SetActive(false);
        }

        public void Menu()
        {
            Credits.SetActive(false);
            Menus.SetActive(true);
        }

        public void Quit()
        {
            Application.Quit();
        }
        
    }
}
