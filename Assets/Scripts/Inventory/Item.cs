﻿using UnityEngine;

namespace Inventory
{
    [CreateAssetMenu(fileName = "Item", menuName = "Inventory/Item")]
    public class Item : ScriptableObject
    {
        public Sprite IventoryItemImage;
        public Texture2D CursorTexture;
        public string MusicKey;
        public AudioClip PickUp;
        public AudioClip WrongPNJ;
        public AudioClip WrongDecor;

    }
}
