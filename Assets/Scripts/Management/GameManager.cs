﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Management
{
    public class GameManager : MonoBehaviour
    {
        public GameObject IntroJeu;
        public GameObject PauseMenu;
        private void Awake()
        {
            PauseMenu.SetActive(false);
        }

        private void Start()
        {
            IntroJeu.SetActive(true);
        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKey(KeyCode.P) || Input.GetKey(KeyCode.Escape))
            {
                Time.timeScale = 0;
                PauseMenu.SetActive(true);
            }
        }

        public void Resume()
        {
            Time.timeScale = 1;
            PauseMenu.SetActive(false);
        }

        public void QuitToMenu()
        {
            SceneManager.LoadSceneAsync(0);
        }
    }
}
