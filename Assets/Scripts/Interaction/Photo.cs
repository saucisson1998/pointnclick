﻿using Player;
using UnityEngine;

namespace Interaction
{
    public class Photo : PickableScript
    {
        public GameObject PhotoCanvas;
        public override void Use(GameObject player)
        {
            base.Use(player);
            player.GetComponent<InteractingComponent>().InUI = true;
            PhotoCanvas.SetActive(true);
        }
    }
}
