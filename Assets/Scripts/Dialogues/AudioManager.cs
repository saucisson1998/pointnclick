﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Dialogues
{
    public class AudioManager : MonoBehaviour
    {

        public Action CallBack;
        public AudioSource AudioPlayer;
        public Queue<AudioClip> AudioClips = new Queue<AudioClip>();
        private AudioClip _currentAudioClip;

        private void Awake()
        {
            AudioPlayer = GetComponent<AudioSource>();
        }

        void Update()
        {
            if (AudioPlayer.clip == null && AudioClips.Count > 0)
            {
                AudioPlayer.clip = AudioClips.Dequeue();
                AudioPlayer.Play();
            }
            else
            {
                if (!AudioPlayer.isPlaying)
                {
                    AudioPlayer.clip = null;
                    if (AudioClips.Count ==0)
                    {
                        CallBack?.Invoke();
                        CallBack = null;
                    }
                }
            }
        }

        public void Add(AudioClip audioClip)
        {
            AudioClips.Enqueue(audioClip);
        }

        public void Stop()
        {
            AudioPlayer.Stop();
        }
        
        public void Skip()
        {
            Stop();
            AudioPlayer.clip = null;
        }

        public void Pause()
        {
            AudioPlayer.Pause();
        }

        public void UnPause()
        {
            AudioPlayer.UnPause();
        }

        public void ClearQueue()
        {
            Skip();
            AudioClips.Clear();
        }
    }
}
