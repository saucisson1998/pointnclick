﻿using Management;
using Player;
using UnityEngine;
using UnityEngine.AI;

namespace Interaction
{
    public class Transition : Interactable
    {
        public GameObject CurrentScene;
        public GameObject NextScene;
        public GameObject NextPosition;

        public MovableComponent MovableComponent;
        public NavigationManager NavigationManager;
        
        public float MinScale;
        public float MultiplierScale;

        private void Start()
        {

        }

        public override void Use(GameObject player)
        {
            CurrentScene.SetActive(false);
            Vector3 playerNewPos = new Vector3(NextPosition.transform.position.x
                , NextPosition.transform.position.y, NextPosition.transform.position.z);
            MovableComponent.MinScale = MinScale;
            MovableComponent.MultiplierScale = MultiplierScale;
            player.transform.position = playerNewPos;
            player.GetComponent<NavMeshAgent>().SetDestination(playerNewPos);
            NextScene.SetActive(true);
            NavigationManager.BakeTerrain();
        }
    }
}