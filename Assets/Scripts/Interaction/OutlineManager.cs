﻿using UnityEngine;

namespace Interaction
{
    public class OutlineManager : MonoBehaviour
    {
        public Material Outline;
        private Material _currentMat;

        void Start()
        {
            _currentMat = GetComponent<SpriteRenderer>().material;
            Outline.mainTexture = _currentMat.mainTexture;
        }

        private void OnMouseEnter()
        {
            GetComponent<SpriteRenderer>().enabled = true;
            GetComponent<SpriteRenderer>().material = Outline;
            GetComponent<SpriteRenderer>().material.shader = Shader.Find("Pick/Outline");
        }

        private void OnMouseExit()
        {
            GetComponent<SpriteRenderer>().enabled = false;
            GetComponent<SpriteRenderer>().material =_currentMat;
        }
    }
}
