﻿using Inventory;
using UnityEngine;

namespace Interaction
{
    public class Tonneau : Interactable
    {
        public Transform SpawnArea;
        private GameObject SpawnedItem;
        public GameObject ItemToSpawn;
        public Inventory.Inventory Inventory;
        public Transform ParentScene;
        public override void Use(GameObject player)
        {
            SpawnedItem = Instantiate(ItemToSpawn, SpawnArea.position, ItemToSpawn.transform.rotation, ParentScene);
            SpawnedItem.GetComponentInChildren<PickableScript>().Inventory = Inventory;
        }
    }
}
