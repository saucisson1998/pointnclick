﻿using Dialogues;
using Player;
using UnityEngine;

namespace Interaction
{
    public class PNJ : Interactable
    {
        public DialogueSO LinkedDialogueSo;
        public GameObject DialogueCanvas;
        public override void Use(GameObject player)
        {
            DialogueCanvas.GetComponent<DialogueManager>().dialogue = LinkedDialogueSo;
            DialogueCanvas.SetActive(true);
            DialogueCanvas.GetComponent<DialogueManager>().player = player;
            player.GetComponent<InteractingComponent>().InUI = true;
        }
        
        //delegate 
    }
}
