﻿using Inventory;
using UnityEngine;

namespace Interaction
{
    public class PickableScript : Interactable
    {
        public Inventory.Inventory Inventory;
        public Item Item;
        
        public override void Use(GameObject player)
        {
            Inventory.AddItem(Item);
            Destroy(transform.parent.gameObject);
        }

    }
}
