using UnityEngine;
using UnityEngine.EventSystems;

namespace Inventory
{
    public class ButtonClick : MonoBehaviour, IPointerClickHandler
    {
        private Inventory _inventory;
        public Item CurrentItem;

        private void Awake()
        {
            _inventory = GetComponentInParent<Inventory>();
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (CurrentItem == null) return;
            if (!(_inventory.CurrentlySelected != CurrentItem && _inventory.CurrentlySelected != null))
            {
                _inventory.SwitchBool();
            }
            //_inventory.SwitchBool();
            Debug.Log(_inventory.IsActive);
            if (_inventory.IsActive)
            {
                _inventory.SelectItem(CurrentItem);
                
            }
            else _inventory.UnselectItem();
        }
    }
}