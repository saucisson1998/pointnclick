﻿using System.Collections.Generic;
using Dialogues;
using UnityEngine;

namespace Music
{
    public class MusicManager : MonoBehaviour
    {
        public AudioClip StartGame;
        public List<AudioClip> Intrigues = new List<AudioClip>(3);
        public Dictionary<string,AudioClip> DicoOfIntrigueSongs = new Dictionary<string, AudioClip>();
        private AudioManager _audioManager;
        // Start is called before the first frame update
        void Awake()
        {
            _audioManager = GetComponent<AudioManager>();
        }

        void Start()
        {
            _audioManager.Add(StartGame);
            foreach (var intrigue in Intrigues)
            {
                DicoOfIntrigueSongs.Add(intrigue.name, intrigue);
            }
        }

        public void SwitchMusic(string key)
        {
            _audioManager.ClearQueue();
            _audioManager.Add(DicoOfIntrigueSongs[key]);
        }
        
    }
}
